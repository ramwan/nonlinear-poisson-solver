#ifndef _DATA_TYPES_CPP_
#define _DATA_TYPES_CPP_

#include <cmath>
#include <Tpetra_CrsMatrix_decl.hpp>
#include <Tpetra_Vector.hpp>

#include "data_types.hpp"

const Scalar length[3] = {56.0, 128.0, 56.0}; // dimensions in nm
const Scalar a = 0.56;

const Scalar z_start = length[2]/2;
const Scalar x_min = 0;
const Scalar y_min = 0;
const Scalar z_min = 0;
const Scalar x_max = length[0] - fmod(length[0], a);
const Scalar y_max = length[1] - fmod(length[1], a);
const Scalar z_max = length[2] - fmod(length[2], a);

// regions of interest
const Scalar source[2][3] = {{a, 61, z_start-0.75},
                             {19, 67, z_start+0.75}};
const Scalar drain[2][3] = {{37, 61, z_start-0.75},
                            {x_max, 67, z_start+0.75}};
const Scalar gate1[2][3] = {{27, a, z_start-0.75},
                            {39, 10, z_start+0.75}};
const Scalar gate2[2][3] = {{19, 119, z_start-0.75},
                            {39, y_max+2, z_start+0.75}};
const Scalar lower_limit[2][3] = {{a, a, a},
                                  {x_max, y_max, a}};
const Scalar upper_limit[2][3] = {{a, a, z_max},
                                  {x_max, y_max, z_max}};

const Scalar guess_all = -0.3; // default: -0.3
const Scalar guess_source = 0.26; // default: 0.26
const Scalar guess_drain = 0.26; // default: 0.26
const Scalar guess_gate1 = 0.278; // default: 0.278
const Scalar guess_gate2 = 0.286; // default 0.286
const Scalar upperbound = -1.13;
const Scalar lowerbound = -1.13;

const int linIterations = 100;
const double convergenceLimit = 1e-8;
const int maxNonlinIterations = 250;

const Scalar q = 1.6e-19;
const Scalar kb = 1.38e-23/q;
const Scalar T = 100e-3;
const Scalar kT = kb * T;
const Scalar h = 6.626e-34;
const Scalar h_cut = h/(2*M_PI);
const Scalar ep0 = 8.85e-12*1e-9 / q;
const Scalar conv = q*1e18;
const Scalar m0 = 9.1e-31 / conv;

const Scalar Eg = 1.13;
const Scalar Ef = 0;
const Scalar eps_si = 11.7*ep0;

const Scalar NcSi = 1.7197e-7;
const Scalar NcSD = 0.8 / gsl_sf_fermi_dirac_half(0.26/kT);
const Scalar NcG1 = 0.83 / gsl_sf_fermi_dirac_half(0.278/kT);
const Scalar NcG2 = 0.84 / gsl_sf_fermi_dirac_half(0.286/kT);
const Scalar nplead = 1.7/1.5;
const Scalar NpSD = nplead;
const Scalar NpG1 = nplead;
const Scalar NpG2 = nplead;

const GO numx = (x_max-x_min)/a + 1;
const GO numy = (y_max-y_min)/a + 1;
const GO numz = (z_max-z_min)/a + 1;

#endif
