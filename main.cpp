#include <Kokkos_Core.hpp>
#include <Tpetra_Core.hpp>
#include <Teuchos_CommandLineProcessor.hpp>
#include <Teuchos_oblackholestream.hpp>
#include <cmath>
#include <iostream>
#include <string>

#include "data_types.hpp"
#include "finite_difference_matrix.hpp"
#include "solver.hpp"

const char *help_string =
"Program for generating various kinds of matrices. For now, this only \
generates ones used in the finite difference method.\n";

int main(int argc, char *argv[])
{
  std::string divider(80, '=');
  divider.append("\n");
  bool serial_node = false;

  Tpetra::ScopeGuard tpetraScope(&argc, &argv);
  {
    using Teuchos::CommandLineProcessor;
    using Teuchos::RCP;
    using Teuchos::rcp;

    auto comm = Tpetra::getDefaultComm();
    const int my_rank = comm->getRank();
    const int num_procs = comm->getSize();
    
    Teuchos::oblackholestream blackhole;
    std::ostream &out = (my_rank == 0 ? std::cout : blackhole);
    out << divider
        << "Running program with " << num_procs
        << (num_procs > 1 ? " processes.\n" : " process.\n")
        << divider << std::endl;;

    out << "x dimensions: " << x_min << " to " << x_max << std::endl;
    out << "x step size:  " << a << std::endl;
    out << "y dimensions: " << y_min << " to " << y_max << std::endl;
    out << "y step size:  " << a << std::endl;
    out << "z dimensions: " << z_min << " to " << z_max << std::endl;
    out << "z step size:  " << a << std::endl;
    out << "Total number of points: " << numx*numy*numz << std::endl;
    out << ((serial_node) ? "Serial " : "Parallel ") << "node creation."
        << std::endl;

    RCP<map_type> global_map =
      rcp(new map_type(numx*numy*numz, 0, comm));
    RCP<const matrix_type> mat =
      create_distributed(comm, global_map, serial_node);
    out << "Matrix created." << std::endl;

    /*
     * ALL ELEMENTS HERE ARE FULLY LOCAL ON EACH PROCESSOR.
     */
    RCP<const multi_vec_type> atoms_grid_loc;
    RCP<const multi_vec_type> source_region_loc;
    RCP<const multi_vec_type> drain_region_loc;
    RCP<const multi_vec_type> gate1_region_loc;
    RCP<const multi_vec_type> gate2_region_loc;
    RCP<const multi_vec_type> upper_region_loc;
    RCP<const multi_vec_type> lower_region_loc;

    atoms_grid_loc = atomGrid(comm, numx, numy, numz, out);
    out << "Atoms grid created." << std::endl;

    source_region_loc =
      getContactRegion(atoms_grid_loc, source, comm);
      out << "source region created." << std::endl;

    drain_region_loc =
      getContactRegion(atoms_grid_loc, drain, comm);
      out << "drain region created." << std::endl;

    gate1_region_loc =
      getContactRegion(atoms_grid_loc, gate1, comm);
      out << "gate1 region created." << std::endl;

    gate2_region_loc =
      getContactRegion(atoms_grid_loc, gate2, comm);
      out << "gate2 region created." << std::endl;

    upper_region_loc =
      getContactRegion(atoms_grid_loc, upper_limit, comm);
      out << "upper region created." << std::endl;

    lower_region_loc =
      getContactRegion(atoms_grid_loc, lower_limit, comm);
      out << "lower region created." << std::endl;

    /* END FULLY LOCAL ELEMENTS */
    atoms_grid_loc = Teuchos::null;

    RCP<vector_type> initial_guess =
      build_initial_guess(source_region_loc, drain_region_loc,
                          gate1_region_loc, gate2_region_loc,
                          upper_region_loc, lower_region_loc, comm);
    
    /* print the matrix if we want it for debugging purposes */
    RCP<std::basic_ostream<char>> stream = Teuchos::rcpFromRef(out);
    const RCP<Teuchos::FancyOStream> f_out = Teuchos::getFancyOStream(stream);

    RCP<vector_type> solution =
      nonlinear_solve(initial_guess, mat, comm, source_region_loc,
                      drain_region_loc, gate1_region_loc, gate2_region_loc,
                      upper_region_loc, lower_region_loc, numx,
                      numy, numz, out);

    /* Solution vector print */
    if(num_procs == 1)
    {
      auto solView = solution->getData();
      for(auto i=0; i<=solution->getMap()->getMaxLocalIndex(); ++i)
      {
        auto x = solView[i];
        out << x << std::endl;
      }
    } else // we need to import our vector to our main process and print it
    {
      typedef Tpetra::Import<LO, GO,
                             NT> import_type;
      RCP<map_type> procZeroMap =
        rcp(new map_type(numx*numy*numz, numx*numy*numz, 0, comm) );
      import_type importer(solution->getMap(), procZeroMap, f_out);

      RCP<vector_type> procZeroSol = rcp(new vector_type(procZeroMap));
      procZeroSol->doImport(*solution, importer, Tpetra::REPLACE);

      auto solView = procZeroSol->getData();
      for(auto i=0; i<=procZeroMap->getMaxLocalIndex(); ++i)
      {
        auto x = solView[i];
        out << x << std::endl;
      }
    }
    /* Solution vector print end */

    Teuchos::TimeMonitor::summarize(out);
    out << "DISCLAIMER: this code is not correct, don't trust it." << std::endl;
  }

  return 0;
}
