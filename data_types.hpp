#ifndef _DATA_TYPES_
#define _DATA_TYPES_

#include <cmath>
#include <gsl/gsl_sf_fermi_dirac.h>
#include <Tpetra_CrsMatrix_decl.hpp>
#include <Tpetra_Vector.hpp>

typedef double Scalar;
typedef int LO;
typedef int GO;
typedef Tpetra::Vector<>::node_type NT;
typedef Tpetra::Map<LO, GO, NT> map_type;
typedef Tpetra::CrsMatrix<Scalar, LO, GO, NT> matrix_type;
typedef Tpetra::Vector<Scalar, LO, GO, NT> vector_type;
typedef Tpetra::MultiVector<Scalar, LO, GO, NT> multi_vec_type;

extern const Scalar q;
extern const Scalar kb;
extern const Scalar T;
extern const Scalar kT;
extern const Scalar h;
extern const Scalar h_cut;
extern const Scalar ep0;
extern const Scalar conv;
extern const Scalar m0;

extern const Scalar Eg;
extern const Scalar Ef;
extern const Scalar eps_si;

extern const Scalar NcSi;
extern const Scalar NcSD;
extern const Scalar NcG1;
extern const Scalar NcG2;
extern const Scalar nplead;
extern const Scalar NpSD;
extern const Scalar NpG1;
extern const Scalar NpG2;

// regions of interest
extern const Scalar length[3]; // dimensions in nm
extern const Scalar a;

extern const Scalar nx;
extern const Scalar ny;
extern const Scalar nz;
extern const Scalar z_start;
extern const Scalar x_min;
extern const Scalar y_min;
extern const Scalar z_min;
extern const Scalar x_max;
extern const Scalar y_max;
extern const Scalar z_max;
extern const GO numx;
extern const GO numy;
extern const GO numz;

extern const Scalar source[2][3];
extern const Scalar drain[2][3];
extern const Scalar gate1[2][3];
extern const Scalar gate2[2][3];
extern const Scalar lower_limit[2][3];
extern const Scalar upper_limit[2][3];

extern const Scalar guess_all;
extern const Scalar guess_source;
extern const Scalar guess_drain;
extern const Scalar guess_gate1;
extern const Scalar guess_gate2;
extern const Scalar upperbound;
extern const Scalar lowerbound;

extern const int linIterations;
extern const double convergenceLimit;
extern const int maxNonlinIterations;

#endif
