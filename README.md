## Nonlinear Poisson Solver in Trilinos

##### Initial setup

- To compile this program, you need to load the `intel-compiler/2020.0.166`, 
`intel-mkl/2020.0.166` and `gsl` modules. Once they're loaded, run 
`cmake CMakeLists.txt` which will generate the relevant makefile. Each time 
you compile now, you can just run `make` and everything will build for you and 
put the output executable in the `bin/` directory.

- To change the executable name, you need to change `poissonsolver` on lines 80 
and 86 of `CMakeLists.txt` to whatever executable name you want it to be.

##### Using the program and making alterations

To use this program, you need to change some dimensions and values at the top of 
`data_types.cpp` and recompile the program. Currently this program *ONLY* 
works with rectangular blocks for gates, sources and drains. If you decide you 
want to try different geometries, let me know :).

- The regions of interest are defined via 2 points (x1, y1, z1) and (x2, y2, z2) 
specifying the 2 far corners of a rectangular prism.

- The `Scalar` type is actually typedefed to `double` in `data_types.hpp`. That 
file also contains a bunch of other typedefs should you be curious.

- To change the number of linear solver iterations, the convergence limit and 
total number of nonlinear iterations, change the values at the bottom of the 
`change this section` section in `data_types.cpp`.

Normally you'd want to use at least 16 CPUs to get a walltime decrease compared 
to the same python program. This is because (in the test case at least) the 
Trilinos version takes many more nonlinear iterations to achieve convergence. 
To use more CPUs, you need to specify more when running the program with `mpi`. 
I've put a sample jobscript called `nonlinearsolve.pbs` in the tests directory. 
Make sure to change `#PBS -l ncpus` as well when you tell mpi to use more cpus. 
There may be some extra parameters and algorithm changes that can help speed up 
processing time a bit. If you want to have a look into them (good luck) you need 
to have a look at `solver.cpp` at the function `nonlinear_solve` (line 133). 
In that you'd probably want to look at anything that's a string and compare with 
the relevant bits [here](https://gitlab.com/ramwan/trilinos-guide/-/blob/master/5_NOX/4_filling_out_main_cpp.md) 
which is part of a long guide on Trilinos linear and non-linear solving I wrote. 
You can also ask me to change things for you :).

The output printed by this program will have a bunch of extra info as well, 
such as timers and solver iteration details. Just go through in a text editor 
and delete those extra chunks to get the vector out.

##### Testing

I've compared the Trilinos solver output to the python solver output for two 
configurations and one set of solution vectors with comparing code are in the 
`test-outputs` directory. If you notice anything funny happening with the results, 
let me know as well :).

You'll notice that the 2norm values of the python and cpp solution vectors are 
very very different but if you end up plotting the result in matlab you'll get 
the same sort of contours. I'm pretty certain that my residual calculating 
function is correct and that the difference actually comes from the Fermi-Dirac 
function used in python and the GSL Fermi-Dirac function.

I've included a cpp and python program in the `tests` directory with the relevant 
outputs in the `test-outputs` directory. The GSL Fermi-Dirac gives larger values 
but both python and GSL functions follow the same shape hence my thoughts on the 
end solution vector differences coming down to this function.

My matlab plotting code which plots x-y plane slices is as follows:

```
% showvec.m
function [] = showvec(slices, range)
  pause on;
  for i=1:range
    image( abs(slices(:, :, i).*(6.^3)) ) % the scaling is arbitrary
    colorbar;
    title(i)
    pause(0.3)
  end
end
% end showvec.m

% given our box dimensions of (56, 128, 56) and 2 solution vectors 
% `cpp` and `pyt`
cppsplit = reshape(cpp, 100, 229, 100);
pytsplit = reshape(pyt, 100, 229, 100);

showvec(cppsplit, 100) % wait for animation to finish
figure(2)
showvec(pytsplit, 100) % wait for animation to finish
```

