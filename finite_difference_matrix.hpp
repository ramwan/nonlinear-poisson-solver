#ifndef _FINITE_DIFFERENCE_MATRIX_
#define _FINITE_DIFFERENCE_MATRIX_

#include <Kokkos_Core.hpp>
#include <Teuchos_RCPDecl.hpp>
#include <Tpetra_Core.hpp>
#include <Tpetra_CrsMatrix.hpp>
#include <Tpetra_Map_decl.hpp>

#include "data_types.hpp"

void globalOrdinalToPoint(const GO g, GO points[3]);

GO pointToGlobalOrdinal(const GO point[3]);

Teuchos::RCP<matrix_type> create_distributed
(const Teuchos::RCP<const Teuchos::Comm<int> >&comm,
 Teuchos::RCP<map_type> global_map,
 const bool serial_node);
#endif
