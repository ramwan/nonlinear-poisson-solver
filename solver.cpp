#include "solver.hpp"

// local grid creation
Teuchos::RCP<const multi_vec_type>
atomGrid(const Teuchos::RCP<const Teuchos::Comm<int> > comm,
         Scalar xtotal, Scalar ytotal, Scalar ztotal,
         std::ostream& out)
{
  using Teuchos::RCP;
  using Teuchos::rcp;

  Scalar total_points = xtotal*ytotal*ztotal;
  // local map
  RCP<const map_type> map =
    rcp(new map_type(total_points, total_points, 0, comm));
  RCP<multi_vec_type> grid = rcp(new multi_vec_type(map, 3));

  Kokkos::MDRangePolicy<Kokkos::Rank<3> >
    policy({a, a, a}, {xtotal+a, ytotal+a, ztotal+a});
  Kokkos::parallel_for("atom grid creation", policy,
    KOKKOS_LAMBDA(LO i, LO j, LO k)
    {
      auto loc = consolidate_coordsLO(i, j, k);

      grid->replaceLocalValue(loc, 0, static_cast<Scalar>(i+1)*a); // x
      grid->replaceLocalValue(loc, 1, static_cast<Scalar>(j+1)*a); // y
      grid->replaceLocalValue(loc, 2, static_cast<Scalar>(k+1)*a); // z
    });

  return grid;
}

// local vector creation
Teuchos::RCP<const multi_vec_type>
getContactRegion
(Teuchos::RCP<const multi_vec_type> atoms,
 const Scalar contact[2][3],
 const Teuchos::RCP<const Teuchos::Comm<int> > comm)
{
  using Teuchos::RCP;
  using Teuchos::rcp;

  Scalar x1 = contact[0][0];
  Scalar x2 = contact[1][0];
  Scalar y1 = contact[0][1];
  Scalar y2 = contact[1][1];
  Scalar z1 = contact[0][2];
  Scalar z2 = contact[1][2];

  Scalar num_atoms = nx * ny * nz;
  Scalar region_x = 0;
  Scalar region_y = 0;
  Scalar region_z = 0;
  auto localAtoms = atoms->get2dView();

  LO region_size = 0;
  for(LO l=0; l < localAtoms[0].size(); l++)
  {
    region_x = localAtoms[0][l];
    region_y = localAtoms[1][l];
    region_z = localAtoms[2][l];

    // if within our specified contact region
    if (region_x >= x1-a/2 && region_x <= x2+a/2 &&
        region_y >= y1-a/2 && region_y <= y2+a/2 &&
        region_z >= z1-a/2 && region_z <= z2+a/2)
    {
      region_size++;
    }
  }

  // local map
  RCP<const map_type> map =
    rcp(new map_type(region_size, region_size, 0, comm));
  RCP<multi_vec_type> region = rcp(new multi_vec_type(map, 3));

  LO i = 0;
  for(LO l=0; l < localAtoms[0].size(); l++)
  {
    region_x = localAtoms[0][l];
    region_y = localAtoms[1][l];
    region_z = localAtoms[2][l];

    // if within our specified contact region
    if (region_x >= x1-a/2 && region_x <= x2+a/2 &&
        region_y >= y1-a/2 && region_y <= y2+a/2 &&
        region_z >= z1-a/2 && region_z <= z2+a/2)
    {
      region->replaceLocalValue(i, 0, localAtoms[0][l]);
      region->replaceLocalValue(i, 1, localAtoms[1][l]);
      region->replaceLocalValue(i, 2, localAtoms[2][l]);
      ++i;
    }
  }

  return region;
}

// distributed vector
Teuchos::RCP<vector_type>
build_initial_guess
(Teuchos::RCP<const multi_vec_type> source_loc,
 Teuchos::RCP<const multi_vec_type> drain_loc,
 Teuchos::RCP<const multi_vec_type> gate1_loc,
 Teuchos::RCP<const multi_vec_type> gate2_loc,
 Teuchos::RCP<const multi_vec_type> upper_loc,
 Teuchos::RCP<const multi_vec_type> lower_loc,
 const Teuchos::RCP<const Teuchos::Comm<int> > comm)
{
  using Teuchos::RCP;
  using Teuchos::rcp;
  
  RCP<const map_type> map = rcp(new map_type(numx*numy*numz, 0, comm));
  RCP<vector_type> guess = rcp(new vector_type(map, 1));
  // GUESS_ALL
  guess->putScalar(guess_all);
  
  LO consol_coord;
  Scalar x, y, z;

  setRegion(guess, source_loc, guess_source);
  setRegion(guess, drain_loc, guess_drain);
  setRegion(guess, gate1_loc, guess_gate1);
  setRegion(guess, gate2_loc, guess_gate2);
  setRegion(guess, upper_loc, upperbound);
  setRegion(guess, lower_loc, lowerbound);
  
  return guess;
}

// will always at least run 1 iteration of the nonlin solve
Teuchos::RCP<vector_type>
nonlinear_solve
(Teuchos::RCP<vector_type> initial_guess,
 Teuchos::RCP<const matrix_type> A,
 const Teuchos::RCP<const Teuchos::Comm<int> >& comm,
 Teuchos::RCP<const multi_vec_type> source_loc,
 Teuchos::RCP<const multi_vec_type> drain_loc,
 Teuchos::RCP<const multi_vec_type> gate1_loc,
 Teuchos::RCP<const multi_vec_type> gate2_loc,
 Teuchos::RCP<const multi_vec_type> upper_loc,
 Teuchos::RCP<const multi_vec_type> lower_loc,
 const Scalar xtotal, const Scalar ytotal, const Scalar ztotal,
 std::ostream &out)
{
  using Teuchos::RCP;
  using Teuchos::rcp;
  using Teuchos::rcp_dynamic_cast;
  using Teuchos::rcp_static_cast;
  typedef Scalar Scalar;
  typedef LO LO;
  typedef GO GO;
  typedef NT Node;

  Teuchos::RCP<EvaluatorTpetra> evaluator = evaluatorTpetra
      (comm, initial_guess->getMap(), A, source_loc, drain_loc, gate1_loc,
       gate2_loc, upper_loc, lower_loc);
  
  Stratimikos::DefaultLinearSolverBuilder builder;
  
  Teuchos::RCP<Teuchos::ParameterList> p = Teuchos::parameterList();
  p->set("Linear Solver Type", "Belos");
  Teuchos::ParameterList& belosList =
    p->sublist("Linear Solver Types").sublist("Belos");
  belosList.set("Solver Type", "Pseudo Block GMRES");
  belosList.sublist("Solver Types").sublist("Pseudo Block GMRES")
      .set<int>("Maximum Iterations", linIterations);
  belosList.sublist("Solver Types").sublist("Pseudo Block GMRES")
      .set<int>("Num Blocks", 10);
  belosList.sublist("Solver Types").sublist("Pseudo Block GMRES")
      .set("Output Frequency", 10);
  belosList.sublist("VerboseObject").set("Verbosity Level", "low");
  p->set("Preconditioner Type", "None");
  builder.setParameterList(p);

  Teuchos::RCP<Thyra::LinearOpWithSolveFactoryBase<Scalar> >
    lowsFactory = builder.createLinearSolveStrategy("");

  RCP<Thyra::TpetraVectorSpace<Scalar, LO, GO, Node> > x_vec_space =
    Thyra::tpetraVectorSpace<Scalar, LO, GO, Node>(initial_guess->getMap());
  RCP<Thyra::TpetraVector<Scalar, LO, GO, Node> > x_vec =
    Thyra::tpetraVector<Scalar, LO, GO, Node>(x_vec_space, initial_guess);

  // jacobian-free operator
  Teuchos::ParameterList printParams;
  RCP<Teuchos::ParameterList> jfnkParams = Teuchos::parameterList();
  jfnkParams->set("Difference Type", "Forward");
  jfnkParams->set("Perturbation Algorithm", "KSP NOX 2001");
  jfnkParams->set("lambda", 1.0e-5);
  RCP<NOX::Thyra::MatrixFreeJacobianOperator<Scalar> > jfnkOp =
    rcp(new NOX::Thyra::MatrixFreeJacobianOperator<Scalar>(printParams));
  jfnkOp->setParameterList(jfnkParams);
  jfnkParams->print(out);

  RCP<Thyra::ModelEvaluator<Scalar> > thyraModel =
    rcp(new NOX::MatrixFreeModelEvaluatorDecorator<Scalar>(evaluator));

  Teuchos::RCP<NOX::Thyra::Group> nox_group =
    Teuchos::rcp(new NOX::Thyra::Group(*x_vec, evaluator, jfnkOp,
                                       lowsFactory, Teuchos::null,
                                       Teuchos::null, Teuchos::null) );
  
  nox_group->computeF();
  
  // VERY IMPORTANT (apparently)
  // the JFNK object needs base evaluation objects. This creates a circular
  // dependency so use a weak pointer.
  jfnkOp->setBaseEvaluationToNOXGroup(nox_group.create_weak());

  // NOX status tests
  RCP<NOX::StatusTest::NormF> absresid =
    rcp(new NOX::StatusTest::NormF(convergenceLimit));
  RCP<NOX::StatusTest::NormWRMS> wrms =
    rcp(new NOX::StatusTest::NormWRMS(1.0e-2, 1.0e-8));
  RCP<NOX::StatusTest::Combo> converged =
    rcp(new NOX::StatusTest::Combo(NOX::StatusTest::Combo::AND));
  converged->addStatusTest(absresid);
  converged->addStatusTest(wrms);
  RCP<NOX::StatusTest::MaxIters> maxiters =
    rcp(new NOX::StatusTest::MaxIters(maxNonlinIterations)); // default 60
  RCP<NOX::StatusTest::FiniteValue> fv =
    rcp(new NOX::StatusTest::FiniteValue);
  RCP<NOX::StatusTest::Combo> combo =
    rcp(new NOX::StatusTest::Combo(NOX::StatusTest::Combo::OR));
  combo->addStatusTest(fv);
  combo->addStatusTest(converged);
  combo->addStatusTest(maxiters);

  // NOX parameter list
  RCP<Teuchos::ParameterList> nl_params = Teuchos::parameterList();
  nl_params->set("Nonlinear Solver", "Line Search Based");
  nl_params->sublist("Direction").sublist("Newton")
      .sublist("Linear Solver").set("Tolerance", 1.0e-6);

  // set output parameters
  nl_params->sublist("Printing").sublist("Output Information").set("Debug", true);
  nl_params->sublist("Printing").sublist("Output Information").set("Warning", true);
  nl_params->sublist("Printing").sublist("Output Information").set("Error", true);
  nl_params->sublist("Printing").sublist("Output Information").set("Test Details", true);
  nl_params->sublist("Printing").sublist("Output Information").set("Details", true);
  nl_params->sublist("Printing").sublist("Output Information").set("Parameters", true);
  nl_params->sublist("Printing").sublist("Output Information").set("Linear Solver Details", true);
  nl_params->sublist("Printing").sublist("Output Information").set("Inner Iteration", true);
  nl_params->sublist("Printing").sublist("Output Information").set("Outer Iteration", true);
  nl_params->sublist("Printing").sublist("Output Information").set("Outer Iteration StatusTest", true);

  // Create the solver
  RCP<NOX::Solver::Generic> solver =
    NOX::Solver::buildSolver(nox_group, combo, nl_params);
  NOX::StatusTest::StatusType solvStatus = solver->solve();

  NOX::Thyra::Group finalGroup =
      dynamic_cast<const NOX::Thyra::Group&>( solver->getSolutionGroup() );
  RCP<const Thyra::VectorBase<Scalar> > solvecbase =
        dynamic_cast<const NOX::Thyra::Vector&>( finalGroup.getX() )
          .getThyraRCPVector();

  typedef Thyra::TpetraOperatorVectorExtraction<Scalar, LO, GO, Node>
    tpetra_extract;
  RCP<Thyra::VectorBase<Scalar> > v =
    Teuchos::rcp_const_cast<Thyra::VectorBase<Scalar> >( solvecbase );
  RCP<vector_type> solution = tpetra_extract::getTpetraVector(v);

  return solution;
}
