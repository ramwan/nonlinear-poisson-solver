#include <gsl/gsl_errno.h>
#include <gsl/gsl_sf_result.h>
#include <gsl/gsl_sf_fermi_dirac.h>

typedef double Scalar;

int main(int argc, char *argv[])
{
  std::cout.precision(8);

  for(int i=0; i<100; i++)
  {
    Scalar test = 0.01 * static_cast<Scalar>(i);
    gsl_sf_result eval;
    gsl_error_handler_t *old_error_handler = gsl_set_error_handler_off();
    int ret = gsl_sf_fermi_dirac_half_e(V, &eval);

    Scalar result;
    if(ret == GSL_SUCCESS)
      result = eval.val;
    else
      result = 0;

    std::cout << result << std::endl;
  }

  return 0;
}
